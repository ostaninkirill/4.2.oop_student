package src.models;

import java.util.Arrays;

public class Group {
    private String groupName;
    private Student[] students;

    public Group(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    public Student[] getStudents() {
        return students;
    }

    public void addStudent(Student[] students) {
        this.students = students;
    }

}
