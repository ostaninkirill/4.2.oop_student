package src.models;

public class Student {
    private String name;
    private String surname;
    private StudentProgress studentProgress;

    public Student(String name, String surname, int[] studentMarks) {
        this.name = name;
        this.surname = surname;
        studentProgress = new StudentProgress();
        studentProgress.setJournal(studentMarks);
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public StudentProgress getStudentProgress() {
        return studentProgress;
    }

}
