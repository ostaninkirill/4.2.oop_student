package src.models;

import java.util.Arrays;

public class StudentProgress {
    private int [] journal = new int[7];

    public StudentProgress() {
    }

    public int[] getJournal() {
        return journal;
    }

    public void setJournal(int[] journal) {
        this.journal = journal;
    }


}
