package src.Service;

import src.models.Group;
import src.models.Student;

public interface IMarksCalculationService {
    double averageMarksStudent(Student student);

    double avarageMarksGroup(Group group);

    int excellentStudent(Group group);

    int loserStudent(Group group);


}
