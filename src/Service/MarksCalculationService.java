package src.Service;

import src.models.Group;
import src.models.Student;

public class MarksCalculationService implements IMarksCalculationService {

    // Средний балл каждого студента
    @Override
    public double averageMarksStudent(Student student) {
        double sumAvarageMarksStudent = 0;
        int countMarksNotNull = 0;
        double avarageMarksStudent = 0;
        int[] marks = student.getStudentProgress().getJournal();
        for (int i = 0; i < marks.length; i++) {
            if (marks[i] == 0){
                continue;
            }
            else {
                countMarksNotNull++;
                sumAvarageMarksStudent += marks[i];
            }
        }
        if(countMarksNotNull != 0) {
            avarageMarksStudent =  Math.round ((sumAvarageMarksStudent / countMarksNotNull) * 100.0)/ 100.0;
        }
      return avarageMarksStudent;
    }

    // Средний балл учебной группы
    @Override
    public double avarageMarksGroup(Group group) {
        double sumAvarageMarksGroup = 0;
        int count = 0;
        double avarageMarksGroup = 0;

        for (Student student : group.getStudents()) {
            if (student != null) {
                sumAvarageMarksGroup += averageMarksStudent(student);
                count++;
            }
        }
        if (count > 0) {
            avarageMarksGroup = Math.round ((sumAvarageMarksGroup / count * 1.0) * 100.0)/ 100.0;
        }

        return avarageMarksGroup;
    }

    // Число отличников в группе
    @Override
    public int excellentStudent(Group group){
        int count = 0;
        for (Student student : group.getStudents()) {
            if ((student != null) && (averageMarksStudent(student) == 5)) {
                count++;
            }
        }
        return count;
    }

    // Количество студентов, имеющих “неудовлетворительно”
    @Override
    public int loserStudent(Group group){
        int count = 0;
        for (Student student : group.getStudents()) {
            int[] marks = student.getStudentProgress().getJournal();
            for (int i = 0; i < marks.length; i++) {
                if (marks[i] == 2){
                    count++;
                    break;
                }
        }
        }
        return count;
    }



}
