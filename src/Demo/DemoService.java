package src.Demo;

import src.Service.IMarksCalculationService;
import src.Service.MarksCalculationService;
import src.models.Group;
import src.models.Student;

public class DemoService implements IDemoService{

    @Override
    public void execute() {
        // Создание первой группы и добавление в неё студентов
        Group groupFirst = new Group("B01");
        Student student11 = new Student("Nikolai", "Kasyanov", new int[] {4,5,4,5,5,5,4});
        Student student12 = new Student("Maksim", "Gavrilov", new int[] {2,4,5,0,2,4,4});
        Student student13 = new Student("Alex", "Sergeev", new int[] {0,0,0,0,0,0,0});
        Student student14 = new Student("Mihail", "Dementev", new int[] {5,5,5,5,2,5,5});

        groupFirst.addStudent(new Student[] {student11, student12, student13, student14});


        // Создание второй группы и добавление в неё студентов
        Group groupSecond = new Group("B02");
        Student student21 = new Student("Roman", "Maksimov", new int[] {4,4,4,4,5,4,4});
        Student student22 = new Student("Anton", "Smirnov", new int[] {4,0,5,2,3,4,4});
        Student student23 = new Student("Daria", "Bokun", new int[] {4,5,4,4,4,3,4});
        Student student24 = new Student("Mihail", "Chegarov", new int[] {5,5,5,5,5,5,5});

        groupSecond.addStudent(new Student[] {student21, student22, student23, student24});


        IMarksCalculationService marksCalculationService = new MarksCalculationService();

        System.out.println("Средний балл в группе " + groupFirst.getGroupName() + " - " + marksCalculationService.avarageMarksGroup(groupFirst));
        System.out.println("Средний балл в группе " + groupSecond.getGroupName() + " - " + marksCalculationService.avarageMarksGroup(groupSecond));
        System.out.println("Средний балл студента " + student14.getName() + " " + student14.getSurname() + " - " + marksCalculationService.averageMarksStudent(student14));
        System.out.println("Количество отличников в группе "  + groupFirst.getGroupName() + " - " + marksCalculationService.excellentStudent(groupFirst));
        System.out.println("Количество отличников в группе "  + groupSecond.getGroupName() + " - " + marksCalculationService.excellentStudent(groupSecond));
        System.out.println("Количество студентов в группе " + groupFirst.getGroupName() + " имеющих \"неудовлетворительно\"" + " - " +  marksCalculationService.loserStudent(groupFirst));
        System.out.println("Количество студентов в группе " + groupSecond.getGroupName() + " имеющих \"неудовлетворительно\"" + " - " + marksCalculationService.loserStudent(groupSecond));
        System.out.println("Средний балл студента " + student24.getName() + " " + student24.getSurname() + " - " + marksCalculationService.averageMarksStudent(student24));
        System.out.println("Средний балл студента " + student11.getName() + " " + student11.getSurname() + " - " + marksCalculationService.averageMarksStudent(student11));

    }
}
